package com.example.demo.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Serv.AficionesServImp;
import com.example.demo.dto.AficionesDTO;

@RestController
@RequestMapping("aficiones")
public class AficionesController {

    private AficionesServImp aficionesServ;

    public AficionesController(AficionesServImp aficionesServ) {
        this.aficionesServ = aficionesServ;
    }
    

    @GetMapping()
    public String bienvenido() {
        return "Hola Mundo!";
    }
    
    @GetMapping(value = "/red/{grupo}/{esquema}",  produces = MediaType.APPLICATION_JSON_VALUE) 
    public ResponseEntity<List<AficionesDTO>>  aficionesEsquema(@PathVariable("grupo") String grupo,@PathVariable("esquema") String esquema){
        return ResponseEntity.ok(aficionesServ.getAllAficiones(grupo,esquema));        
    }

    @GetMapping(value = "/red/{cadena}",  produces = MediaType.APPLICATION_JSON_VALUE) 
    public ResponseEntity<List<AficionesDTO>>  aficiones(@PathVariable("cadena") String cadena){
        return ResponseEntity.ok(aficionesServ.getAllAficiones(cadena));        
    }

    @GetMapping(value = "/red/nombreData") 
    public  String aficiones() {
        return aficionesServ.getNombreData();
    }

    @GetMapping(value = "/red/nombreEmpresa") 
    public  String getNomempresa() {
        return aficionesServ.getNombreEmpresa();
    }



}
