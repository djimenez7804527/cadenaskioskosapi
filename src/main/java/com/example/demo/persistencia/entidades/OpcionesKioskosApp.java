package com.example.demo.persistencia.entidades;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "OPCIONESKIOSKOSAPP")
public class OpcionesKioskosApp {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SECUENCIA")
    private Long secuencia;

    @Size(min = 1, max = 20)
    @Column(name = "CODIGO")
    private String codigo;
    
    @Size(min = 1, max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    
    
    @Size(max = 1000)
    @Column(name = "AYUDA")
    private String ayuda;

    @Size(max = 100)
    @Column(name = "ICONO")
    private String icono;
    
    @Size(min = 1, max = 20)
    @Column(name = "CLASE")
    private String clase;

    @Size(max = 50)
    @Column(name = "NOMBRERUTA")
    private String nombreruta;

    @Size(max = 1)
    @Column(name = "REQDESTINO")
    private String reqDestino;

    @JoinColumn(name = "OPCIONKIOSKOPADRE", referencedColumnName = "SECUENCIA")
    @ManyToOne
    private OpcionesKioskosApp opcionkioskopadre;

    @JoinColumn(name = "EMPRESA", referencedColumnName = "SECUENCIA")
    @ManyToOne
    private Empresas empresa;

    @JoinColumn(name = "KIOROL", referencedColumnName = "SECUENCIA")
    @ManyToOne
    private KioRoles kiorol;

}
