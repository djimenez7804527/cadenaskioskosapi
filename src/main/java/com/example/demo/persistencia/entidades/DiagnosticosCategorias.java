package com.example.demo.persistencia.entidades;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "DIAGNOSTICOSCATEGORIAS")
public class DiagnosticosCategorias {
    
    @Id
    @Column(name = "SECUENCIA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long secuencia;
    
    @Column(name = "CODIGO")
    private String codigo;
    
    @Size(max = 200)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    
}
