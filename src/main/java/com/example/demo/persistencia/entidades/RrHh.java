package com.example.demo.persistencia.entidades;

import java.math.BigInteger;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RrHh {
    
    @Id
    @Column(name = "SECUENCIA")
    private BigInteger secuencia;

    @Column(name = "TITULO")
    private String titulo;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "NOMBREADJUNTO")
    private String nombreadjunto;

    @Column(name = "FECHAINICIO")
    private String fechainicio;

    @Column(name = "FECHAFIN")
    private String fechafin;

    @Column(name = "FECHAINICIO1")
    private String fechainicio1;

    @Column(name = "FECHAFIN1")
    private String fechafin1;

    @Column(name = "FORMATO")
    private String formato;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "FECHAMODIFICADO")
    private String fechamodificado;

    @Column(name = "FECHACREACION")
    private String fechacreacion;

    @Column(name = "FECHACREACION1")
    private String fechacreacion1;

}
