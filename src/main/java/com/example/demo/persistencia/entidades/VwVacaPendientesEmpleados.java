package com.example.demo.persistencia.entidades;

import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "VWVACAPENDIENTESEMPLEADOS")
public class VwVacaPendientesEmpleados {

    @Id
    @Column(name = "RFVACACION")
    private BigDecimal rfVacacion;

    @JoinColumn(name = "EMPLEADO", referencedColumnName = "SECUENCIA")
    @ManyToOne(optional = false)
    private Empleados empleado;

    @Column(name = "INICIALCAUSACION")
    @Temporal(TemporalType.DATE)
    private Date inicialCausacion;
    
    @Column(name = "FINALCAUSACION")
    @Temporal(TemporalType.DATE)
    private Date finalCausacion;
    @Column(name = "DIASPENDIENTES")
    private BigDecimal diasPendientes;

    @Transient
    private String periodoCausado;

    @Transient
    private BigDecimal diasreales;


}
