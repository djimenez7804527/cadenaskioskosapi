package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.example.demo.config",
				"com.example.demo.controller",
				"com.example.demo.dto","com.example.demo.persistencia.entidades",
				"com.example.demo.persistencia.dao.cadenask",
				"com.example.demo.persistencia.dao.repoDAO",
				"com.example.demo.Serv"})
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
