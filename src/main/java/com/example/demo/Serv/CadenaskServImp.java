package com.example.demo.Serv;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CadenaskDTO;
import com.example.demo.persistencia.dao.cadenask.CadenaskDAO;
import com.example.demo.persistencia.entidades.CadenaskEntity;

@Service
public class CadenaskServImp implements CadenaskServ {

    @Autowired
    private CadenaskDAO cadenaskDAO;

    @Override
    public List<CadenaskDTO> getAllCadenas() {

        List<CadenaskDTO> lista = new ArrayList<>();

        cadenaskDAO.findAll().forEach(a -> lista.add(entityToDto(a)));
        return lista;
    }

    @Override
    public List<CadenaskDTO> getCadenasKioskos(String grupo) {
        List<CadenaskDTO> lista = new ArrayList<>();

        cadenaskDAO.getCadenasKioskos(grupo).forEach(a -> lista.add(entityToDto(a)));

        return lista;
    }

    
    @Override
    public CadenaskDTO entityToDto(CadenaskEntity p) {
        CadenaskDTO ca = new CadenaskDTO(p.getSecuencia(), p.getCodigo(), p.getDescripcion(), p.getCadena(),
                p.getNitempresa(), p.getGrupo(), p.getEmplnomina(), p.getEsquema(), p.getEstado(), p.getObservacion(),
                p.getDominio());
        return ca;
    }


}
