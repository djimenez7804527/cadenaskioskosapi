package com.example.demo.dto;



import jakarta.persistence.Id;


public class CadenaskDTO {
    @Id
    
    private long secuencia;
    private String codigo;
    private String descripcion;
    private String cadena;
    private int nitempresa;
    private String grupo;
    private String emplnomina;
    private String esquema;
    private String estado;
    private String observacion;
    private String dominio;

	public CadenaskDTO() {
	}

	public CadenaskDTO(long secuencia, String codigo, String descripcion, String cadena, int nitempresa,
			String grupo, String emplnomina, String esquema, String estado, String observacion, String dominio) {
		this.secuencia = secuencia;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.cadena = cadena;
		this.nitempresa = nitempresa;
		this.grupo = grupo;
		this.emplnomina = emplnomina;
		this.esquema = esquema;
		this.estado = estado;
		this.observacion = observacion;
		this.dominio = dominio;
	}

	public long getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(long secuencia) {
		this.secuencia = secuencia;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCadena() {
		return cadena;
	}

	public void setCadena(String cadena) {
		this.cadena = cadena;
	}

	public int getNitempresa() {
		return nitempresa;
	}

	public void setNitempresa(int nitempresa) {
		this.nitempresa = nitempresa;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getEmplnomina() {
		return emplnomina;
	}

	public void setEmplnomina(String emplnomina) {
		this.emplnomina = emplnomina;
	}

	public String getEsquema() {
		return esquema;
	}

	public void setEsquema(String esquema) {
		this.esquema = esquema;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

    
    
}
      
