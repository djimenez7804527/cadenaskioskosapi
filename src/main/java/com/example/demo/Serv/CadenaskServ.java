package com.example.demo.Serv;

import java.util.List;

import com.example.demo.dto.CadenaskDTO;
import com.example.demo.persistencia.entidades.CadenaskEntity;



public interface CadenaskServ {
    public List<CadenaskDTO> getAllCadenas();
    public CadenaskDTO entityToDto(CadenaskEntity p);
    List<CadenaskDTO> getCadenasKioskos(String grupo); 
}
