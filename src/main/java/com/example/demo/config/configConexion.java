package com.example.demo.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
// @PropertySource({ "classpath:persistence-multiple-db.properties" })
@EnableJpaRepositories(basePackages = "com.example.demo.persistencia.dao.cadenask", 
    entityManagerFactoryRef = "cadenaskEntityManager", 
    transactionManagerRef = "cadenaskTransactionManager")
public class configConexion {
  @Autowired
  private Environment env;

  @SuppressWarnings("null")
  @Bean
  @Primary
  public LocalContainerEntityManagerFactoryBean cadenaskEntityManager() throws NamingException {

    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();

    em.setDataSource(cadenaskDataSource());
    em.setPackagesToScan(new String[] { "com.example.demo.persistencia.entidades" }); // MAPEA ENTIDAD

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

    em.setJpaVendorAdapter(vendorAdapter);    
    return em;
  }

  @Primary
  @Bean
  public DataSource cadenaskDataSource() throws NamingException {

    @SuppressWarnings("null")
    DataSource dataSource = (DataSource) new JndiTemplate().lookup(env.getProperty("spring.datasource.DEFAULT.jndi-name"));
    return dataSource;
  }

  @Primary
  @Bean
  public PlatformTransactionManager cadenaskTransactionManager() throws NamingException {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(cadenaskEntityManager().getObject());
    return transactionManager;
  }
}
