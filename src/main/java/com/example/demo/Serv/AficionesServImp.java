package com.example.demo.Serv;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.AficionesDTO;
import com.example.demo.persistencia.dao.repoDAO.AficionesDAO;
import com.example.demo.persistencia.entidades.AficionesEntity;

@Service
public class AficionesServImp implements AficionesServ {
    
    @Autowired
    private AficionesDAO aficionesDAO;
    
   
    @Override
    public List<AficionesDTO> getAllAficiones(String cadena,String esquema){
        aficionesDAO.setCadena(cadena,esquema);
        List<AficionesDTO> listaAficiones = new ArrayList<>();

        aficionesDAO.getAll().forEach(a -> listaAficiones.add(entityToDto(a)));
        return listaAficiones;
    }

    @Override
    public List<AficionesDTO> getAllAficiones(String cadena){
        aficionesDAO.setCadena(cadena);
        List<AficionesDTO> listaAficiones = new ArrayList<>();

        aficionesDAO.getAll().forEach(a -> listaAficiones.add(entityToDto(a)));
        return listaAficiones;
    }

    public String getNombreData(){
        return aficionesDAO.getnombreDataSource();
    } 

    public String getNombreEmpresa(){
        return aficionesDAO.nombreEmpresa();
    } 
    
    @Override
    public AficionesDTO entityToDto(AficionesEntity p){
        AficionesDTO per= new AficionesDTO(p.getSecuencia(),p.getCodigo(),p.getDESCRIPCION()); 
        return per;
    }

    

}
