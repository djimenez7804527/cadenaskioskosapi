package com.example.demo.persistencia.entidades;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CAUSASAUSENTISMOS")
public class CausasAusentismos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long secuencia;

    @Size(min = 1, max = 4)
    @Column(name = "CODIGO")
    private int codigo;

    @Size(min = 1, max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;    

    @JoinColumn(name = "CLASE", referencedColumnName = "SECUENCIA")
    @ManyToOne(optional = false)
    private ClasesAusentismos clase;

    
    public CausasAusentismos(Long secuencia){
        this.secuencia = secuencia;
    }



}

