package com.example.demo.Serv;

import java.sql.SQLException;
import java.util.List;

import com.example.demo.dto.AficionesDTO;
import com.example.demo.persistencia.entidades.AficionesEntity;

public interface AficionesServ {
    public List<AficionesDTO> getAllAficiones(String grupo,String esquema) throws SQLException;
    public List<AficionesDTO> getAllAficiones(String grupo) throws SQLException;
    public AficionesDTO entityToDto(AficionesEntity p);
}
