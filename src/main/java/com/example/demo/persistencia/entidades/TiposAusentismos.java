package com.example.demo.persistencia.entidades;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TIPOSAUSENTISMOS")
public class TiposAusentismos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SECUENCIA")
    private Long secuencia;

    @Size(min = 1, max = 4)
    @Column(name = "CODIGO")
    private String codigo;
    
    @Size(min = 1, max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;

}
