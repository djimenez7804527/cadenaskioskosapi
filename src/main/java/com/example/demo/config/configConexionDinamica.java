package com.example.demo.config;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiTemplate;
import org.springframework.stereotype.Component;

@Component
public class configConexionDinamica {
    
    @Autowired
    private Environment env;

    private Connection connection;
    private String dataSourceName;
    private ResultSet resultSet;
    private String esquema = "";

    public String getEsquema() {
        return esquema;
    }



    public void setEsquema(String esquema) {
        this.esquema = esquema;
    }



    public void setDataSourceName (String dataSourceName) {
        this.dataSourceName = "spring.datasource."+ dataSourceName+".jndi-name";
    }

    

    @SuppressWarnings("null")
    public String getDataSourceName () {
        return env.getProperty(dataSourceName);
    }
    
    public Connection getConnection() {
        try {
            @SuppressWarnings("null")
            DataSource dataSource = (DataSource) new JndiTemplate().lookup(env.getProperty(dataSourceName));
            connection = dataSource.getConnection();

            String query = "SET ROLE ROLKIOSKO" + this.esquema + " IDENTIFIED BY RLKSK"; 
            System.out.println("query seteo : " + query); 

            
            Statement statement = connection.createStatement(); 
            statement.executeQuery(query);  
            

        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void cerrarConexion() throws SQLException{
        this.connection.close();
    }



    public void ejecutar(String query) {
        try {            
            Connection connection = getConnection();            
            Statement statement = connection.createStatement();            
            this.resultSet = statement.executeQuery(query);       
            cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    
}
