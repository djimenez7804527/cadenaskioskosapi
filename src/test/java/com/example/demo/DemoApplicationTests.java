package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes =  DemoApplicationTests.class)
class DemoApplicationTests {

	@Test
	void contextLoads() {
		String holamundo = "Hola mundo";
		System.out.print(holamundo + ". ");
	}
	
}
