package com.example.demo.persistencia.entidades;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CLASESAUSENTISMOS")
public class ClasesAusentismos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SECUENCIA")
    private Long secuencia;

    @Size(min = 1, max = 4)
    @Column(name = "CODIGO")    
    private String codigo;

    @Size(min = 1, max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    @JoinColumn(name = "TIPO", referencedColumnName = "SECUENCIA")
    @ManyToOne(optional = false)
    private TiposAusentismos tipo;

    
    public ClasesAusentismos(Long secuencia){
        this.secuencia = secuencia;
    }
    
}
