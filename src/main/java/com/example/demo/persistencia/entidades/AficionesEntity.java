package com.example.demo.persistencia.entidades;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AFICIONES")
public class AficionesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long secuencia;
	
	private int codigo;
    private String DESCRIPCION;


}
