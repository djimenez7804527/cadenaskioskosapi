package com.example.demo.persistencia.entidades;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CADENASKIOSKOSAPP")
public class CadenaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long secuencia;
    private String codigo;
    private String descripcion;
    private String cadena;
    private int nitempresa;
    private String grupo;
    private String emplnomina;
    private String esquema;
    private String estado;
    private String observacion;
    private String dominio;

    
}
      
