package com.example.demo.persistencia.entidades;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "KIOCAUSASAUSENTISMOS")
public class KioCausasAusentismos {
    
    @Id
    @Column(name = "SECUENCIA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long secuencia;
    
    @Size(min = 1, max = 20)
    @Column(name = "CODIGO")
    private String codigo;

    @JoinColumn(name = "CAUSAAUSENTISMO", referencedColumnName = "SECUENCIA")
    @ManyToOne(optional = false)
    private CausasAusentismos causa;

    @JoinColumn(name = "EMPRESA", referencedColumnName = "SECUENCIA")
    @ManyToOne(optional = false)
    private Empresas empresa;
    
    @Size(max = 1000)
    @Column(name = "OBSERVACION")
    private String observacion;

    
}
