package com.example.demo.dto;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AficionesDTO {
    @Id
    private long secuencia;
    private int codigo;
    private String DESCRIPCION;

   
}
