package com.example.demo.persistencia.entidades;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "KIOROLES")
public class KioRoles {
    
    @Id
    @Column(name = "SECUENCIA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long secuencia;
    
    @Column(name = "CODIGO")
    private String codigo;
    
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Size(max = 100)
    @Column(name = "OBSERVACION")
    private String observacion;

   
}
