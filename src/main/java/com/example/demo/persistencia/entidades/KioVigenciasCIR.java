package com.example.demo.persistencia.entidades;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "KIOVIGENCIASCIR")
public class KioVigenciasCIR {
    
    @Id
    @Column(name = "SECUENCIA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long secuencia;
    
    @JoinColumn(name = "OPCIONKIOSKOAPP", referencedColumnName = "SECUENCIA")
    @ManyToOne
    private OpcionesKioskosApp opcionkioskoapp;
    
    @JoinColumn(name = "EMPRESA", referencedColumnName = "SECUENCIA")
    @ManyToOne
    private Empresas empresa;
    @Column(name = "ANO")
    private int ano;

    @Column(name = "ESTADO")
    private String estado;
    
    @Column(name = "ANOARCHIVO")
    private int anoArchivo;

}
