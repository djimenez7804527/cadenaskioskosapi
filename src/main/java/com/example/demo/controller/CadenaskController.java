package com.example.demo.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Serv.CadenaskServImp;
import com.example.demo.dto.CadenaskDTO;

import jakarta.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("cadena")
public class CadenaskController {
    
    private CadenaskServImp cadenaskServ;

    public CadenaskController(CadenaskServImp cadenaskServImp){
        this.cadenaskServ = cadenaskServImp;
    }

    @GetMapping()
    public String bienvenido() {
        return "Hola mundo!";
    }
    
    @GetMapping(value="/verCadenas", produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<List<CadenaskDTO>> Hola() {
        return ResponseEntity.ok(cadenaskServ.getAllCadenas());
    }

    @GetMapping(value="/grupo/{grupo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<List<CadenaskDTO>> getCadenasKioskos(@PathVariable("grupo") String grupo) {        
        return ResponseEntity.ok(cadenaskServ.getCadenasKioskos(grupo));
    }

    @RequestMapping(value="/redirect/{grupo}")
    public  void redireccinar(HttpServletResponse response, @PathVariable("grupo") String grupo) throws IOException {        
        List<CadenaskDTO> lista = cadenaskServ.getCadenasKioskos(grupo);

        String def ="";
        if(lista.get(0).getEsquema() != null){
            def = "../../aficiones/red/" + lista.get(0).getCadena() + "/" + lista.get(0).getEsquema() ;
        }else{
            def = "../../aficiones/red/" + lista.get(0).getCadena();
        }

        
        System.out.println("redirigiendo a: " + def);
        response.sendRedirect(def);
    }


   

}
