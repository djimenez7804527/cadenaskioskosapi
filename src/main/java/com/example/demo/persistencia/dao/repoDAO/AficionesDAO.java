package com.example.demo.persistencia.dao.repoDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.persistencia.entidades.AficionesEntity;
import com.example.demo.config.configConexionDinamica;

@Repository
public class AficionesDAO{

    @Autowired
    private configConexionDinamica configConexionDinamica  = new configConexionDinamica();
    

    public void setCadena(String cadena,String esquema) {       
        
       this.configConexionDinamica.setDataSourceName(cadena);
       this.configConexionDinamica.setEsquema(esquema);
    } 

    public void setCadena(String cadena) {     
        String esquema = "";  
        this.configConexionDinamica.setEsquema(esquema);
        this.configConexionDinamica.setDataSourceName(cadena);
     } 
    
    public String getnombreDataSource(){
        return configConexionDinamica.getDataSourceName();
    }

    public List<AficionesEntity> getAll(){

        List<AficionesEntity> listAficiones = new ArrayList<>();
        String query = "SELECT * FROM aficiones";
        configConexionDinamica.ejecutar(query);       

        try {
            while (configConexionDinamica.getResultSet().next()) {
                
                    listAficiones.add(new AficionesEntity(configConexionDinamica.getResultSet().getLong("secuencia"),
                        configConexionDinamica.getResultSet().getInt("codigo"),
                        configConexionDinamica.getResultSet().getString("descripcion")));                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listAficiones;
    }




    public String nombreEmpresa(){

        String nombres = "";

        try {
            
            Connection connection = configConexionDinamica.getConnection();
            
            Statement statement = connection.createStatement();
            
            ResultSet resultSet = statement.executeQuery("SELECT nombre FROM empresas where codigo != 0");
            
            while (resultSet.next()) {
                nombres += (resultSet.getString("nombre")) + " \b";
            }
            


            configConexionDinamica.cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombres;
    }

   
    public AficionesEntity getAficionesById(Long secuencia) {

        AficionesEntity aficionesEntity = null;
        try {
            
            Connection connection = configConexionDinamica.getConnection();
            
            Statement statement = connection.createStatement();
            
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Aficiones WHERE secuencia = " + secuencia);
            
            if (resultSet.next()) {
                aficionesEntity = new AficionesEntity(resultSet.getLong("secuencia"), resultSet.getInt("codigo"),resultSet.getString("descripcion"));
            }
            
            configConexionDinamica.cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aficionesEntity;
    }
    
}