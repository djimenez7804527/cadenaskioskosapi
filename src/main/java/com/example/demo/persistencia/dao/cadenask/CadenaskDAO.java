package com.example.demo.persistencia.dao.cadenask;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.persistencia.entidades.CadenaskEntity;

//import org.springframework.data.jpa.repository.Query;



public interface CadenaskDAO extends JpaRepository<CadenaskEntity, Long>{
    
    @Query("SELECT c FROM CadenaskEntity c WHERE c.grupo = ?1")
    List<CadenaskEntity> getCadenasKioskos(String grupo); 
    
}