package com.example.demo.persistencia.entidades;

import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CONEXIONESKIOSKOS")
public class ConexionesKioskos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long secuencia;

    @Column(name = "SEUDONIMO")
    private String seudonimo;

    @Column(name = "ACTIVO")
    private String activo;
    
    @Column(name = "ULTIMACONEXION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaconexion;

    @Column(name = "FECHADESDE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechadesde;

    @Column(name = "FECHAHASTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechahasta;
    
    @Column(name = "ENVIOCORREO")
    private String enviocorreo;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "DIRIGIDOA")
    private String dirigidoa;

    @Column(name = "NITEMPRESA")
    private long nitEmpresa;

    @Column(name = "PERSONA")
    private BigDecimal persona;

}
